import Component from "@glimmer/component";
import { tracked } from "@glimmer/tracking";
import { getOwner } from "@ember/application";
import { hash } from "@ember/helper";
import PluginOutlet from "discourse/components/plugin-outlet";

export default class HomepageBlocks extends Component {
  @tracked blocks = [];

  <template>
    <div class="homepage-blocks">
      <PluginOutlet @name="before-homepage-blocks" />
      {{#each this.blocks as |block|}}
        <PluginOutlet
          @name="before-homepage-block"
          @outletArgs={{hash block=block}}
        />
        {{component block.name params=block.parsedParams}}
        <PluginOutlet
          @name="after-homepage-block"
          @outletArgs={{hash block=block}}
        />
      {{/each}}
      <PluginOutlet @name="after-homepage-blocks" />
    </div>
  </template>

  constructor() {
    super(...arguments);

    const blocksArray = [];

    JSON.parse(settings.blocks).forEach((block) => {
      if (getOwner(this).hasRegistration(`component:${block.name}`)) {
        block.classNames = `homepage-block`;
        block.parsedParams = {};
        if (block.params) {
          block.params.forEach((p) => {
            block.parsedParams[p.name] = p.value;
          });
        }
        blocksArray.push(block);
      } else {
        // eslint-disable-next-line no-console
        console.warn(
          `The component "${block.name}" was not found, please update the configuration on the Homepage Blocks editor.`
        );
      }
    });

    this.blocks = blocksArray;
  }
}
