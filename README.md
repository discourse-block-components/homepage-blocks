# Homepage Blocks

Adds ability to display components as blocks on the custom homepage route. On the settings editor you choose the blocks to display, adjust their ordering and add optional parameters.

You can use any Ember component as a block, you just need to use the correct name. For example, core includes a `signup-cta` Ember component, and you can use it as is.
